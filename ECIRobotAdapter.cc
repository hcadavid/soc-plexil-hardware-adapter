

#include "ECIRobotAdapter.hh"

#include "subscriber.hh"
#include "ECIRobotSystemHandler.hh"

#include "AdapterConfiguration.hh"
#include "AdapterFactory.hh"
#include "AdapterExecInterface.hh"
#include "Debug.hh"
#include "StateCacheEntry.hh"

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;
using std::map;
using std::string;
using std::vector;
using std::copy;


///////////////////////////// Conveniences //////////////////////////////////

// A preamble for error messages.
static string error = "Error in ECIRobotAdapter: ";

// A prettier name for the "unknown" value.
static Value Unknown;

// A localized handle on the adapter, which allows a
// decoupling between the sample system and adapter.
static ECIRobotAdapter * Adapter;

// An empty argument vector.
static vector<Value> EmptyArgs;


///////////////////////////// State support //////////////////////////////////

// Queries the system for the value of a state and its arguments.
//
static Value fetch (const string& state_name, const vector<Value>& args)
{
  debugMsg("ECIRobotAdapter:fetch",
           "Fetch called on " << state_name << " with " << args.size() << " args");
  Value retval;

  // NOTE: A more streamlined approach to dispatching on state name
  // would be nice.

  if (state_name == "Speed"){
    retval = getSpeed();
  }
  else if (state_name == "LeftSonarMeasuredDistance"){
    retval = getLeftSonarMeasuredDistance();
  }
  else if (state_name == "RightSonarMeasuredDistance"){
    retval = getRightSonarMeasuredDistance();
  }
  else if (state_name == "CenterSonarMeasuredDistance"){
    retval = getCenterSonarMeasuredDistance();
  }
  else if (state_name == "Temperature"){
    retval = getTemperature();
  }
  else if (state_name == "WheelStuck"){
      retval = getWheelStuck();
  }
  else if (state_name == "Latitude"){
      retval = getLatitude();
  }
  else if (state_name == "Longitude"){
      retval = getLongitude();
  }
  else if (state_name == "Heading"){
      retval = getHeading();
  }
  else if (state_name == "PositionChanged"){                          
      retval = getPositionChanged();
  }
  else if (state_name == "StartRequested"){                          
      retval = getStartRequested();
  }
  else if (state_name == "AbortRequested"){                          
      retval = getAbortRequested();
  }  
  
  
  else {
    cerr << error << "invalid state: [" << state_name << "]" << endl;
    retval = Unknown;
  }

  debugMsg("ECIRobotAdapter:fetch", "Fetch returning " << retval);
  return retval;
}


// The 'receive' functions are the subscribers for system state updates.  They
// receive the name of the state whose value has changed in the system.  Then
// they propagate the state's new value to the executive.

static void propagate (const State& state, const vector<Value>& value)
{
  Adapter->propagateValueChange (state, value);
}

static State createState (const string& state_name, const vector<Value>& value)
{
  State state(state_name, value.size());
  if (value.size() > 0)
  {
    for(size_t i=0; i<value.size();i++)
    {
      state.setParameter(i, value[i]);
    }
  }
  return state;
}

static void receive (const string& state_name, int val)
{
  propagate (createState(state_name, EmptyArgs),
             vector<Value> (1, val));
}

static void receive (const string& state_name, float val)
{
  propagate (createState(state_name, EmptyArgs),
             vector<Value> (1, val));
}

static void receive (const string& state_name, const string& val)
{
  propagate (createState(state_name, EmptyArgs),
             vector<Value> (1, val));
}

static void receive (const string& state_name, bool val, const string& arg)
{
  propagate (createState(state_name, vector<Value> (1, arg)),
             vector<Value> (1, val));
}

static void receive (const string& state_name, bool val, int arg1, int arg2)
{
  vector<Value> vec;
  vec.push_back (arg1);
  vec.push_back (arg2);
  propagate (createState(state_name, vec), vector<Value> (1, val));
}


///////////////////////////// Member functions //////////////////////////////////


ECIRobotAdapter::ECIRobotAdapter(AdapterExecInterface& execInterface,
                             const pugi::xml_node& configXml) :
    InterfaceAdapter(execInterface, configXml)
{
  debugMsg("ECIRobotAdapter", " created.");
}

bool ECIRobotAdapter::initialize()
{
  g_configuration->defaultRegisterAdapter(this);
  Adapter = this;
  setSubscriberInt (receive);
  setSubscriberReal (receive);
  setSubscriberString (receive);
  setSubscriberBoolString (receive);
  setSubscriberBoolIntInt (receive);
  debugMsg("ECIRobotAdapter", " initialized.");
  
  //setup(initialize) communication interface  
  if (!initializeCommunications()){
      return false;
  }
  
  //start thread that receives events from comm interface
  startLookupEventsThread(); 
  //startStatusPollingThread();  
  return true;
}

bool ECIRobotAdapter::start()
{
  debugMsg("ECIRobotAdapter", " started.");
  return true;
}

bool ECIRobotAdapter::stop()
{
  debugMsg("ECIRobotAdapter", " stopped.");
  return true;
}

bool ECIRobotAdapter::reset()
{
  debugMsg("ECIRobotAdapter", " reset.");
  return true;
}

bool ECIRobotAdapter::shutdown()
{
  debugMsg("ECIRobotAdapter", " shut down.");
  return true;
}


// Sends a command (as invoked in a Plexil command node) to the system and sends
// the status, and return value if applicable, back to the executive.
//
void ECIRobotAdapter::executeCommand(Command *cmd)
{
  const string &name = cmd->getName();
  debugMsg("ECIRobotAdapter", "Received executeCommand for " << name);  

  Value retval = Unknown;
  vector<Value> argv(10);
  const vector<Value>& args = cmd->getArgValues();
  copy (args.begin(), args.end(), argv.begin());

  // NOTE: A more streamlined approach to dispatching on command type
  // would be nice.
  string s;
  
  //int32_t i1 = 0, i2 = 0;
  
  double d = 0.0;

  if (name == "PlantSeed") {
    retval=plantSeed();
  }   
  else if (name == "TurnFrontWheels") {    
    args[0].getValue(d);
    retval=turnFrontWheels(d);
  }  
  else if (name == "TurnRearWheels") {    
    args[0].getValue(d);
    retval=turnRearWheels(d);
  }  
  else if (name == "MoveForward") {    
    args[0].getValue(d);
    moveForward(d);
  }
  else if (name == "MoveBackward") {    
    args[0].getValue(d);
    moveBackward(d);
  }
  else if (name == "MoveSprinklerToLeft") {    
    moveSprinklerToLeft();
  }
  else if (name == "MoveSprinklerToRight") {    
    moveSprinklerToRight();
  }
  else if (name == "OpenSprinkler") {    
      openSprinkler();
  }
  else if (name == "CloseSprinkler") {    
      closeSprinkler();
  }
  else if (name == "PlantSeed") {    
    plantSeed();
  }
  else if (name == "Stop") {    
    stopEngine();
  }
  else 
    cerr << error << "invalid command: [" << name << "]" << endl;

  // This sends a command handle back to the executive.
  m_execInterface.handleCommandAck(cmd, COMMAND_SENT_TO_SYSTEM);
  // This sends the command's return value (if expected) to the executive.
  if (retval != Unknown)
    m_execInterface.handleCommandReturn(cmd, retval);
  m_execInterface.notifyOfExternalEvent();
}

void ECIRobotAdapter::lookupNow(State const &state, StateCacheEntry &entry)
{
  // This is the name of the state as given in the plan's LookupNow
  string const &name = state.name();
  const vector<Value>& args = state.parameters();
  entry.update(fetch(name, args));
}


void ECIRobotAdapter::subscribe(const State& state)
{  
  debugMsg("ECIRobotAdapter:subscribe", " processing state "
           << state.name());
  m_subscribedStates.insert(state);
}


void ECIRobotAdapter::unsubscribe (const State& state)
{
  debugMsg("ECIRobotAdapter:subscribe", " from state "
           << state.name());
  m_subscribedStates.erase(state);
}

// Does nothing.
void ECIRobotAdapter::setThresholds (const State& state, double hi, double lo)
{
}

void ECIRobotAdapter::setThresholds (const State& state, int32_t hi, int32_t lo)
{
}


void ECIRobotAdapter::propagateValueChange (const State& state,
                                          const vector<Value>& vals) const
{    
  if (!isStateSubscribed(state)){
    cout << "[ERROR] Value changes propagation failed:" << state << "\n";
      return; 
  }
  else{      
    m_execInterface.handleValueChange(state, vals.front());
    m_execInterface.notifyOfExternalEvent();      
  }
    
}


bool ECIRobotAdapter::isStateSubscribed(const State& state) const
{
  return m_subscribedStates.find(state) != m_subscribedStates.end();
}

// Necessary boilerplate
extern "C" {
  void initECIRobotAdapter() {
    REGISTER_ADAPTER(ECIRobotAdapter, "ECIRobotAdapter");
  }
}
